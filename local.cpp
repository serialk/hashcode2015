#include <string>
#include <iostream>
#include <vector>
#include <cstring>
#include <cstdlib>
#include <algorithm>

using namespace std;

int R, S, U; // rangées, largeur, indispo
int P, M; // groupes, serveurs
int r[100], s[100]; // rangée, colonne
int z[1000], c[1000]; // taille, capacité
int indispo[16][101]; // last : nombre d'indispo

int sol[16][101]; // last : nombre de serveur
int group[1000];

int used[1000];

void parse() {
  cin >> R >> S >> U >> P >> M;
  for(int i =0; i < U; i ++) {
    cin >> r[i] >> s[i];
  }
  for(int i =0; i < M; i ++) {
    cin >> z[i] >> c[i];
    cerr << i << " " << z[i] << " " << c[i] << endl;
  }
  memset(sol, -1, sizeof(int[16][101]));
  memset(indispo, -1, sizeof(int[16][101]));
  memset(used, 0, sizeof(int[1000]));
  for(int i = 0; i < U; i++) {
    indispo[r[i]][100]++;
    indispo[r[i]][indispo[r[i]][100]] = s[i];
  }
  for(int i = 0; i < 16; i++) {
    indispo[i][100]++;
    sol[i][100] = 0;
    sort(indispo[i], indispo[i]+indispo[i][100]);
  }
  /*cerr << "debut\n";
  for(int i = 0; i < 16; i++) {
    cerr << "row " << i << "\t";
    for(int j = 0; j < indispo[i][100]; j++)
      cerr << indispo[i][j] << " ";
    cerr << endl;
  }
  cerr << "fin";*/
}


int solcol[1000];
int solrow[1000];

void inverse() {
  for(int row=0; row < 16; row++) {
    int col = 0;
    int nextindispo = 0;
    for(int i=0; i < sol[row][100]; i++) {
      int t = indispo[row][nextindispo];
      int nextsize = z[sol[row][i]];

      if(t >= 0 && col + nextsize > t) // sur l'indispo
      {
        nextindispo++;
        col = t;
      }
      col += nextsize;
      solcol[sol[row][i]] = col;
      solrow[sol[row][i]] = row;
    }
  }
}

void print() {
  inverse();
  for(int i = 0; i < M; i++) {
    if(used[i])
      cout << solrow[i] << " " << solcol[i] << " " << group[i] << endl;
    else
      cout << "x" << endl;
  }
}

int cost() {
  cerr << "TODO\n";
}

// retourne la taille de la rangée l
int sizel(int l) {
  int *row = sol[l];
  int size = 0;
  int nextindispo = 0;
  for(int i = 0; i < row[100]; i++) {
    // indispo ?
    int t = indispo[l][nextindispo];
    int nextsize = z[row[i]];
    if(t >= 0 && size + nextsize > t) // sur l'indispo
    {
      nextindispo++;
      size = t;
    }
    size += nextsize;
  }
  return size;
}

void alea() {
  for(int i =0; i < M; i ++) {
    int l = rand()%16;
    sol[l][sol[l][100]] = i;
    sol[l][100]++;
    used[i] = 1;
    group[i] = rand()%45;
  }
}

int check() {
  inverse();
  for(int l =0; l < 16; l ++) {
    int *row = sol[l];
    int col = 0;
    for(int i = 0; i < row[100]; i++) {
      // indispo ?
      int pos = solrow[row[i]];
      int size = z[row[i]];
      for(int ind = 0; ind < indispo[l][100]; ind++) {
        int t = indispo[l][ind];
        if(t > 0 && t >= pos && t < pos + size)
        {
          cout << l << " a " << i << endl;
          return 0;
          }
      }
      if( i < row[100]-1 && pos+size >= solrow[row[i+1]])
      {
        cout << l << " a " << i << endl;
        return 0;
        }
      }
  }
  return 1;
}


int solve() {
  alea();
  for(int i = 0; i < 16; i++) {
    while(sizel(i) > 100) {
      sol[i][100]--;
      used[sol[i][sol[i][100]]] = 0;
      sol[i][sol[i][100]] = -1;
    }
  }
}

int main() {
  srand(0); //time(NULL));
  parse();
  solve();
  cout << check() << endl;
//  print();
}

