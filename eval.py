#!/usr/bin/python3

import time
import collections
import sys
import random
import datetime

assign = {}

indispo = set()
servers = {}
R, S, U, P, M = list(map(int, input().split()))
for i in range(U):
    indispo.add(tuple(map(int, input().split())))
for i in range(M):
    servers[i] = tuple(map(int, input().split()))

# False si plein, True si vide
slots_map = [[(j, i) not in indispo for i in range(S)] for j in range(R)]

def score(assign):
    matrix = [[0] * R for _ in range(P)]
    for i, line in enumerate(assign):
        if line:
            row, _, group = line
            # print(group, P, row, R, i)
            matrix[group][row] += servers[i][1]
    guarantee = []
    for line in matrix:
        guarantee.append(sum(line) - max(line))
    return guarantee

groups = {}
assign = [None] * M
with open('%s.out' % sys.argv[1]) as f:
    for i, line in enumerate(f):
        if line != 'x\n':
            assign[i] = list(map(int, line.split()))
            groups.setdefault(assign[i][2], []).append(i)

print(min(score(assign)))


def backup(assign):
    with open('theend-%s-%d.out' % (datetime.datetime.strftime(datetime.datetime.now(), '%H%M%S'), min(score(assign))), 'w') as f:
        for i in range(M):
            if assign[i]:
                f.write(' '.join(map(str, assign[i])))
                f.write('\n')
            else:
                f.write('x\n')

for _ in range(10):
    guarantee = score(assign)
    fort, fort_i = max((gua, i) for i, gua in enumerate(guarantee))
    faible, faible_i = min((gua, i) for i, gua in enumerate(guarantee))
    s_fort = random.choice(groups[fort_i])
    assign[s_fort][2] = faible_i
    print(s_fort, 'groupe', faible_i)
    print(min(score(assign)))
    backup(assign)

