#!/usr/bin/python3

import time
import collections
import sys

assign = {}

indispo = set()
servers = {}
R, S, U, P, M = list(map(int, input().split()))
for i in range(U):
    indispo.add(tuple(map(int, input().split())))
for i in range(M):
    servers[i] = tuple(map(int, input().split()))

# False si plein, True si vide
slots_map = [[(j, i) not in indispo for i in range(S)] for j in range(R)]

def print_map():
    for i in range(R):
        print(''.join(map(lambda x: '.' if x else 'x', slots_map[i])))

def taille_max(rangee):
    m = 0
    c = 0
    ci = 0
    mi = None
    for i, e in enumerate(slots_map[rangee]):
        if not e:
            if m < c:
                m = c
                mi = ci
            c = 0
            ci = i + 1
        else:
            c += 1
    if m < c:
        m = c
        mi = ci
    return m, mi

slots_count = {i: taille_max(i) for i in range(R)}

def update_slots(rangee, start, length):
    for i in range(length):
        slots_map[rangee][i + start] = False
    slots_count[rangee] = taille_max(rangee)

capacities = collections.Counter()
capacities_groups = {i: collections.Counter() for i in range(P)}

group = 0
count = 0
for si, (l, cap) in sorted(servers.items(), key=lambda x: x[1][1] ** float(sys.argv[1]) / x[1][0]):
    possible = list(filter(lambda x: x[1][0] >= l, slots_count.items()))
    if not possible:
        continue
    rmin, (_, start) = min(possible, key=lambda x: capacities_groups[group][x[0]])
    #rmin, (_, start) = min(possible, key=lambda x: capacities[x[0]])
    assign[si] = (rmin, start, group)
    capacities[rmin] += cap
    capacities_groups[group][rmin] += cap
    update_slots(rmin, start, l)
    group = (group + 1) % P

    #print()
    #print()
    #print(si)
    #count += 1
    #print_map()
    #time.sleep(1)


for i in range(M):
    try:
        print(' '.join(map(str, assign[i])))
    except KeyError:
        print('x')
